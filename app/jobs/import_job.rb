require 'csv'
class ImportJob < ApplicationJob
  queue_as :default

  def perform(*args)
    # Do something later
    csv_data = CSV.read(args.first, encoding: 'ISO-8859-1', headers: true, col_sep: ';')

    # Import intercommunalities
    import_inter_communalities(csv_data)

    import_communes(csv_data)
  end

  private

  def import_inter_communalities(csv_data)
    csv_data.each do |row|
        Intercommunality.find_or_create_by!(siren: row['siren_epci']) do |new_inter_com|
        new_inter_com.name = row['nom_complet']
        new_inter_com.form = row['form_epci'].downcase[0..2]
      end
    end
  end

  def import_communes(csv_data)
    csv_data.each do |row|
      Commune.find_or_create_by!(code_insee: row['insee']) do |new_com|
        new_com.name = row['nom_com']
        new_com.population = row['pop_total']
        new_com.intercommunality = Intercommunality.find_by_siren(row['siren_epci'])
      end
    end
  end
end
