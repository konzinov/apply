class Street < ApplicationRecord
  has_many :communes

  validates :title, presence: true
  validates :from, numericality:  {allow_nil: true}
  validates :to, numericality: {allow_nil: true, greater_than: :from}
end
