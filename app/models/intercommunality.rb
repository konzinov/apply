class Intercommunality < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: :slugged
  has_many :communes

  validates :name, presence: true
  validates :siren, presence: true, uniqueness: { case_sensitive: false}
  validates :form, inclusion: { in: %w(ca cu cc met)}

  validate :check_siren



  def check_siren
    siren = self.siren.to_i
    digits = num_digits siren
    errors.add(:siren, 'Unexpected length, should be 9 digits') unless digits == 9
    errors.add(:siren, 'Invalid siren number') unless siren_control_number % 10 == 0
    rescue => e
      logger.error e.message
      errors.add(:siren, 'Could not convert siren number into digits')
  end

  # siren control number is computed by multiplying even rank digits by 2 and odd ones by 1
  # after just make a sum of all those digits (same for 2 digits number eg: 18 = 1 + 8)
  # valid siren has a siren control number multiple of 10

  def siren_control_number
    siren_control_number = 0
    first_control = []
    siren_digits = self.siren.chars.map(&:to_i)
    siren_digits.each_with_index do |digit, index|
      first_control << if (siren_digits.length - index) % 2 == 0
                         digit * 2
                       else
                         digit
                       end
    end
    second_control = first_control.map(&:to_s).map(&:chars).flatten.map(&:to_i)
    second_control.each { |digit| siren_control_number += digit }
    siren_control_number
  end

  def num_digits siren
    Math.log10(siren).to_i + 1
  end

  def communes_hash
    communes_hash = {}
    self.communes.each do |commune|
      communes_hash[commune.code_insee] = commune.name
    end
    communes_hash
  end

  def population
    total = 0
    communes.each { |commune| total += commune.population}
    total
  end
end
