class Commune < ApplicationRecord
  belongs_to :intercommunality
  has_many :streets

  validates :name, presence: true
  validates :code_insee, presence: true, length: {is: 5}

  class << self
    def search(commune_name)
      where('lower(name) like ?', "%#{sanitize_sql_like(commune_name.mb_chars.downcase)}%")
    end

    def to_hash
      communes_hash = {}
      all.each do |commune|
        communes_hash[commune.code_insee] = commune.name
      end
      communes_hash
    end
  end
end
