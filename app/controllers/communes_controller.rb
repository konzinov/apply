class CommunesController < ApplicationController
  def index
    respond_to do |format|
      format.json {render json: Commune.all, status: :ok}
      format.html {render html: 'Not acceptable' ,status: :not_acceptable}
    end
  end

  def create
    head :forbidden
  end

  def show
    @commune = Commune.find_by(code_insee: params[:id])
    status = @commune ? :ok : :not_found
    head status
  end

  def update
    @commune = Commune.find_by(code_insee: params[:id])
    if @commune
      begin
        @commune.update(commune_params)
      rescue ActionController::ParameterMissing
        head :bad_request
      end
    else
      head :not_found
    end
  end

  private

  def commune_params
    params.require(:commune).permit(:name, :code_insee)
  end
end
